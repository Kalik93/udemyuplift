﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UdemyUplift.Models;

namespace UdemyUplift.DataAccess.Data.Repository.Interfaces
{
    public interface IFrequencyRepository : IRepository<Frequency>
    {
        void Update(Frequency frequency);
        IEnumerable<SelectListItem> GetFrequencyListForDropDown();
    }
}
