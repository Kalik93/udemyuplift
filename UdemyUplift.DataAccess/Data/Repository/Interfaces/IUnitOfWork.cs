﻿using System;
using System.Collections.Generic;
using System.Text;

namespace UdemyUplift.DataAccess.Data.Repository.Interfaces
{
    public interface IUnitOfWork : IDisposable
    {
        ICategoryRepository Category { get; }
        IFrequencyRepository Frequency { get; }
        void Save();
    }
}
