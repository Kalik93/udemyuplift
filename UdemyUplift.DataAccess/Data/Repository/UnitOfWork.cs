﻿using System;
using System.Collections.Generic;
using System.Text;
using UdemyUplift.DataAccess.Data.Repository.Interfaces;

namespace UdemyUplift.DataAccess.Data.Repository
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly ApplicationDbContext _db;
        public ICategoryRepository Category { get; private set; }
        public IFrequencyRepository Frequency { get; private set; }

        
        public UnitOfWork(ApplicationDbContext  db)
        {
            _db = db;
            Category = new CategoryRepository(_db);
            Frequency = new FrequencyRepository(_db);
        }
    

        public void Dispose()
        {
            _db.Dispose();
        }

        public void Save()
        {
            _db.SaveChanges();
        }
    }
}
