﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UdemyUplift.DataAccess.Data.Repository.Interfaces;
using UdemyUplift.Models;

namespace UdemyUplift.DataAccess.Data.Repository
{
    public class FrequencyRepository : Repository<Frequency>, IFrequencyRepository
    {
        ApplicationDbContext _db;

        public FrequencyRepository(ApplicationDbContext db) : base(db)
        {
            _db = db;
        }
        public IEnumerable<SelectListItem> GetFrequencyListForDropDown()
        {
            return _db.Frequency.Select(i => new SelectListItem()
            {
                Text = i.Name,
                Value = i.Id.ToString()
            });
        }

        public void Update(Frequency frequency)
        {
            var objFromDb =_db.Frequency.Find(frequency.Id);
            if(objFromDb != null)
            {
                objFromDb.Name = frequency.Name;
                objFromDb.FrequencyCount = frequency.FrequencyCount;
            }
            _db.SaveChanges();
        }
    }
}
