﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using UdemyUplift.DataAccess.Data.Repository.Interfaces;
using UdemyUplift.Models;

namespace UdemyUplift.Areas.Admin.Controllers
{
    [Area("Admin")]
    public class FrequencyController : Controller
    {
        private readonly IUnitOfWork _unitOfWork;

        public FrequencyController(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Upsert(int? id)
        {
            var frequency = new Frequency();
            if(id == null)
            {
                return View(frequency);

            }

            frequency = _unitOfWork.Frequency.Get(id.GetValueOrDefault());
            if(frequency == null)
            {
                return NotFound();
            }

            return View(frequency);
        }

        #region API Calls

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Upsert(Frequency frequency)
        {
            if(ModelState.IsValid)
            {
                if(frequency.Id == 0)
                {
                    _unitOfWork.Frequency.Add(frequency);
                }
                else
                {
                    _unitOfWork.Frequency.Update(frequency);
                }

                _unitOfWork.Save();
                return RedirectToAction(nameof(Index));
            }

            return View(frequency);
        }

        [HttpGet]
        public IActionResult GetAllFrequencies() => Json(new { data = _unitOfWork.Frequency.GetAll() });

        [HttpDelete]
        public IActionResult DeleteFrequency(int id)
        {
            var objFromDatabase = _unitOfWork.Frequency.Get(id);
            if(objFromDatabase == null)
            {
                return Json(new { success = false, message = "Could not find such frequency" });
            }

            _unitOfWork.Frequency.Remove(objFromDatabase);
            _unitOfWork.Save();

            return Json(new { success = true, message = $"Successfully deleted Frequency named {objFromDatabase.Name}. " });
        }
        
        #endregion
    }
}