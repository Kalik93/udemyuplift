﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using UdemyUplift.DataAccess.Data.Repository.Interfaces;
using UdemyUplift.Models;

namespace UdemyUplift.Areas.Admin.Controllers
{
    [Area("Admin")]
    public class CategoryController : Controller
    {

        private readonly IUnitOfWork _unitOfWork;

        public CategoryController(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Upsert(int? id)
        {
            var category = new Category();
            if(id == null)
            {
                return View(category);
            }

            category = _unitOfWork.Category.Get(id.GetValueOrDefault());
            if(category == null)
            {
                return NotFound();
            }

            return View(category);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Upsert(Category category)
        {
            if (ModelState.IsValid)
            {
                if(category.Id == 0)
                {
                    _unitOfWork.Category.Add(category);
                }
                else
                {
                    _unitOfWork.Category.Update(category);
                }

                _unitOfWork.Save();
                return RedirectToAction(nameof(Index));
            }

            return View(category);
        }

        #region API CALLS

        [HttpGet]
        public IActionResult GetAllCategories()
        {
            return Json(new { data = _unitOfWork.Category.GetAll()});
        }

        [HttpDelete]
        public IActionResult DeleteCategory(int id)
        {
            var objFromDatabase = _unitOfWork.Category.Get(id);
            if(objFromDatabase == null)
            {
                return Json(new { success = false, message = "Error while deleting. Category with specified ID does not exist." });
            }

            _unitOfWork.Category.Remove(objFromDatabase);
            _unitOfWork.Save();

            return Json(new { success = true, message = $"Object with ID {objFromDatabase.DisplayOrder} removed succesfully" });
        }

        #endregion
    }
}